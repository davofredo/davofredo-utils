package com.davofredo.util;

/**
 * This class handles operations with Strings representing a collection of items, separated by one or more characters.
 *
 */
public class ListUtil {

    /**
     * Creates a String list containing the string representation of the elements contained in the provided array.
     *
     * @param array The array to be represented by the string list.
     * @return A String list containing the string representation of the elements contained in the provided array.
     */
    public static String toStringList(Object[] array) {
        return toStringList(array, false, false);
    }

    /**
     * Creates a String list containing the string representation of the elements contained in the provided array.
     *
     * @param array The array to be represented by the string list.
     * @param allowNullValues If true, null values will appear in the list represented by the "null" word.
     * @param allowEmptyValues If true, empty strings will be counted in the list, like in the example: "item1, item2, , item4".
     * @return A String list containing the string representation of the elements contained in the provided array.
     */
    public static String toStringList(Object[] array, boolean allowNullValues, boolean allowEmptyValues) {
        return toStringList(array, null, allowNullValues, allowEmptyValues);
    }

    /**
     * Creates a String list containing the string representation of the elements contained in the provided array.
     *
     * @param array The array to be represented by the string list.
     * @param listSeparator A String separator between list elements, if null, the elements will be separated by ", ".
     * @return A String list containing the string representation of the elements contained in the provided array.
     */
    public static String toStringList(Object[] array, String listSeparator) {
        return toStringList(array, listSeparator, false, false);
    }

    /**
     * Creates a String list containing the string representation of the elements contained in the provided array.
     *
     * @param array The array to be represented by the string list.
     * @param listSeparator A String separator between list elements, if null, the elements will be separated by ", ".
     * @param allowNullValues If true, null values will appear in the list represented by the "null" word.
     * @param allowEmptyValues If true, empty strings will be counted in the list, like in the example: "item1, item2, , item4".
     * @return A String list containing the string representation of the elements contained in the provided array.
     */
    public static String toStringList(Object[] array, String listSeparator, boolean allowNullValues, boolean allowEmptyValues) {
        if(array == null) return ""; // Avoid throwing unnecessary exceptions, if the list is null, return empty
        if(listSeparator == null) listSeparator = ", "; // Default separator

        StringBuffer stringList = new StringBuffer();
        for(int i = 0; i < array.length; i++) {
            Object o = array[i];
            // Are we allowing null values?
            if(o == null)
                if(allowNullValues) o = "null";
                else continue;
            // Are we allowing empty strings
            else if(!allowEmptyValues && o.toString().trim().length() == 0) continue;
            // If this is not the first element, prepend the list separator
            if(stringList.length() > 0 || (allowEmptyValues && i > 0)) stringList.append(listSeparator);
            // Concat the string representation of the item
            stringList.append(o);
        }

        return stringList.toString();
    }

}
