package com.davofredo.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ListUtilTest {

    private static final String[] STRING_ARRAY = {
        "column1",
        "column2",
        "column3"
    };

    @Test
    void testStringArrayToCommaSeparatedList() {
        String columnList = ListUtil.toStringList(STRING_ARRAY);
        Assertions.assertEquals("column1, column2, column3", columnList);
    }

    @Test
    void testNullListSeparator() {
        String columnList = ListUtil.toStringList(STRING_ARRAY, null);
        Assertions.assertEquals("column1, column2, column3", columnList);
    }

    @Test
    void testNullArrayToStringList() {
        String columnList = ListUtil.toStringList(null);
        Assertions.assertEquals(0, columnList.length());
    }

    @Test
    void testNullArrayToStringListWithSeparator() {
        String columnList = ListUtil.toStringList(null, " |");
        Assertions.assertEquals(0, columnList.length());
    }

    @Test
    void testEmptyArrayToStringList() {
        String columnList = ListUtil.toStringList(new String[0]);
        Assertions.assertEquals(0, columnList.length());
    }

    @Test
    void testNullArrayItemsToStringList() {
        String columnList = ListUtil.toStringList(new Object[] {null, null, null, null});
        Assertions.assertEquals(0, columnList.length());
    }

    @Test
    void testNullArrayItemsToStringListWithNullsIncluded() {
        String columnList = ListUtil.toStringList(new Object[] {null, null, null, null}, true, false);
        Assertions.assertEquals("null, null, null, null", columnList);
    }

    @Test
    void testEmptyArrayItemsToStringListWithEmptyIncluded() {
        String columnList = ListUtil.toStringList(new Object[] {"", "", "", ""}, false, true);
        Assertions.assertEquals(", , , ", columnList);
    }

    @Test
    void testEmptyArrayItemsToStringList() {
        String columnList = ListUtil.toStringList(new Object[] {"", "", "", ""});
        Assertions.assertEquals(0, columnList.length());
    }

    @Test
    void testArrayWithNullItemsToStringList() {
        String columnList = ListUtil.toStringList(new String[] {null, "Item1", null, "Item3","Item4", null, "Item6", null});
        Assertions.assertEquals("Item1, Item3, Item4, Item6", columnList);
    }

    @Test
    void testArrayWithEmptyItemsToStringList() {
        String columnList = ListUtil.toStringList(new String[] {"", "Item1", "", "Item3", "Item4", "", "Item6", ""});
        Assertions.assertEquals("Item1, Item3, Item4, Item6", columnList);
    }

    @Test
    void testArrayWithEmptyAndNullItemsToStringList() {
        String columnList = ListUtil.toStringList(new String[] {null, "Item1", "", "Item3", "Item4", null, "Item6", ""});
        Assertions.assertEquals("Item1, Item3, Item4, Item6", columnList);
    }

    @Test
    void testStringArrayToPipeSeparatedStringList() {
        String columnList = ListUtil.toStringList(STRING_ARRAY, " | ");
        Assertions.assertEquals("column1 | column2 | column3", columnList);
    }

}
